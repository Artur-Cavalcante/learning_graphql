const {gql , GraphQLClient} = require('graphql-request');

async function main(){
    // const endpoint = 'http://127.0.0.1:3000'
    // const graphQLClient = new GraphQLClient(endpoint)

    const endpoint = 'https://api.graph.cool/simple/v1/movies'

    const graphQLClient = new GraphQLClient(endpoint)

    const query = gql`
            {
      Movie(title: "Inception") {
        releaseDate
        actors {
          name
        }
      }
    }
    `
    
    const data = await graphQLClient.request(query)
    console.log(data)
}

main().catch((error)=> console.log('err', error))