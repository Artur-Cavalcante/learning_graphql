const providers = {
    contratos: []
}

let id = 0;

const resolversAdministracao = {
    contrato({ id }){
        return providers.contratos.find(item => item.id === Number(id));
    },
    contratos(){
        return providers.contratos;
    },
    criarContrato({nome, idade, filial}){
        const contrato = {
            id: id++,
            nome,
            idade,
            filial
        };
        providers.contratos.push(contrato);
    
        return contrato;
    }
}

module.exports = resolversAdministracao;