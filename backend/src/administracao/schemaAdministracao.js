const { buildSchema } = require('graphql');

const schemaAdministracao = buildSchema(`
    type Contrato {
        id: ID
        nome: String
        idade: Int
        filial: String
    }

    type Query {
        contrato(id: Int!): Contrato
        contratos: [Contrato]
    }

    type Mutation {
        criarContrato(nome: String!, idade: Int!, filial: String!): Contrato
    }
`)

module.exports = schemaAdministracao