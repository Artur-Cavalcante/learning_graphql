const { buildSchema } = require('graphql');

const schemaAutenticacao = buildSchema(`
    type Token {
        token: Int
    }

    type Query{
        token: Int
    }

    type Mutation {
        login(usuario: String!, senha: String!): Token
    }
`)

module.exports = schemaAutenticacao