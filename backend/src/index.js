const express = require('express');
const cors = require('cors')

const {graphqlHTTP} = require('express-graphql');

/*Schemas*/
const schemaAdministracao = require('./administracao/schemaAdministracao.js')
const resolversAdministracao = require('./administracao/resolversAdministracao.js')

const schemaAutenticacao = require('./autenticacao/schemaAutenticacao.js')
const resolversAutenticacao = require('./autenticacao/resolversAutenticacao.js')

const schemaDevops = require('./devops/schemaDevops.js')
const resolversDevops = require('./devops/resolversDevops.js')

const schemaGeral = require('./geral/schemaGeral.js')
const resolversGeral = require('./geral/resolversGeral.js')

const schemaVeiculo = require('./veiculo/schemaVeiculo.js')
const resolversVeiculo = require('./veiculo/resolversVeiculo.js')
/*Schemas*/

const app = express();

app.use(cors({origin: '*'}))

app.use(
    "/administracao",
    graphqlHTTP({
        schema: schemaAdministracao,
        rootValue: resolversAdministracao,
        graphiql: true,
    })
)
app.use(
    "/devops",
    graphqlHTTP({
        schema: schemaDevops,
        rootValue: resolversDevops,
        graphiql: true,
    })
)
app.use(
    "/geral",
    graphqlHTTP({
        schema: schemaGeral,
        rootValue: resolversGeral,
        graphiql: true,
    })
)
app.use(
    "/autenticacao",
    graphqlHTTP({
        schema: schemaAutenticacao,
        rootValue: resolversAutenticacao,
        graphiql: true,
    })
)
app.use(
    "/veiculo",
    graphqlHTTP({
        schema: schemaVeiculo,
        rootValue: resolversVeiculo,
        graphiql: true,
    })
)




app.listen(3000, () => {console.log('Server running!')});